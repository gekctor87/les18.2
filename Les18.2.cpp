﻿#include <iostream>
#include <typeinfo>

using namespace std;

template <class T>
class Stack
{
private:
	int SStack = 1;
	int top;
	T* DArray;

public:
	Stack()
	{
		DArray = new T[SStack];
		top = 0;
	}

	~Stack()
	{
		delete[] DArray;
		DArray = nullptr;
	}

	void push(T e1)
	{
		if (top < SStack)
		{
			DArray[top++] = e1;
		}
		else
		{
			T* NewArray = new T[++SStack];
			for (int i = 0; i < SStack - 1; i++)
			{
				NewArray[i] = DArray[i];
			}
			delete[] DArray;
			DArray = NewArray;
			DArray[top++] = e1;
		}
	}

	T pop()
	{
		if (top >= 0)
		{
			return DArray[--top];
		}
		else return 0;
	}

	void show()
	{
		cout << "Stack: ";
		for (int i = 0; i < top; i++)
		{
			cout << DArray[i] << " ";
		}
		cout << endl;
	}
	int size()
	{
		return SStack;
	}
};

int main()
{
	Stack<float> MyStack;

	MyStack.push(5);
	MyStack.push(4);
	MyStack.push(3);
	MyStack.push(2);
	MyStack.push(1);

	MyStack.show();

	cout << MyStack.pop() << endl;
	cout << MyStack.pop() << endl;

	MyStack.show();
}